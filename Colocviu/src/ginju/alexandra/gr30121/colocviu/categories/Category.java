package ginju.alexandra.gr30121.colocviu.categories;

import ginju.alexandra.gr30121.colocviu.entities.Product;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Category {
    private int id;
    private String name;

    private String outputFile = "outputFile.txt";

    public void appendToFile(Product product, String outputFile){

        try(FileWriter fw = new FileWriter(outputFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(product);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addProduct(Product p){

        appendToFile(p, outputFile);
    }

    public void removeProduct(Product p){

    }

    public void updateProduct(Product p){

    }

    public void getProduct(int id){

    }
}


