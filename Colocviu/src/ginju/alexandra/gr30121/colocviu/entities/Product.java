package ginju.alexandra.gr30121.colocviu.entities;

import ginju.alexandra.gr30121.colocviu.actions.*;
import ginju.alexandra.gr30121.colocviu.categories.Category;

import java.util.ArrayList;

public class Product extends ProductDetails implements SellProduct, RetireProduct {
    private boolean forSale;
    private int categoryId;

    public Product(){

    }

    public Product(int id, String name, int price, boolean forSale, int categoryId){

        super(id, name, price);
        this.forSale = forSale;
        this.categoryId = categoryId;
    }

    public void markForSale() {

    }

    public void retireFromSale() {

    }
}

class ProductDetails {
    private int id;
    private String name;
    private int price;

    public ProductDetails(){

    }

    public ProductDetails(int id, String name, int price) {

        this.id = id;
        this.name = name;
        this.price = price;
    }
}

class Site {
    private String name;
    private String url;
    private ArrayList<Category> categories = new ArrayList<Category>();

    public Site(String name, String url) {

    }

    public void initCategories() {

    }
}
