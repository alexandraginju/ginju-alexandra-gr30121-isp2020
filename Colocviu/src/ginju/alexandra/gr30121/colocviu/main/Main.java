package ginju.alexandra.gr30121.colocviu.main;

import ginju.alexandra.gr30121.colocviu.categories.Category;
import ginju.alexandra.gr30121.colocviu.entities.Product;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

        char raspuns;
        String linie, linie1, explic;

        Category category = new Category();

        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader in = new BufferedReader(
                new FileReader("deschidereaplic.txt"));
        PrintWriter out = new PrintWriter(
                new BufferedWriter(new FileWriter("inchidereaplic.txt")));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga produs");
            System.out.println("s - Sterge produs");
            System.out.println("e - editeaza produs");
            System.out.println("p - Pune produs la vanzare");
            System.out.println("r - Retrage produs");
            System.out.println("i - Iesi");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);


            switch(raspuns) {
                case 'a':
                    System.out.println("Introduceti produs:");
                    linie1=in.readLine();
                    String []tokens = linie1.split(" ");
                    int id = Integer.valueOf(tokens[0]);
                    String name = tokens[1];
                    int price = Integer.valueOf(tokens[2]);
                    boolean forSale = Boolean.parseBoolean(tokens[3]);
                    int categoryId = Integer.parseInt(tokens[4]);
                    category.addProduct(new Product(id, name, price, forSale, categoryId));
                    out.println(linie);
                    out.close();
                    System.out.println("Produse adaugate");
                    break;
                case 's': case 'S':
                    break;
                case 'e': case 'E':

                    break;
                case 'p': case 'P':
                    break;
                case 'r': case 'R':
                    break;
            }
        } while(raspuns!='i' && raspuns!='I');
        System.out.println("Program terminat.");
    }
}



