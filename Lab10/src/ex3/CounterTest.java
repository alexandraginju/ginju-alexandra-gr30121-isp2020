package ex3;

public class CounterTest {
    public static void main(String[] args) throws InterruptedException {
        Counter1 c1=new Counter1();
        Counter2 c2=new Counter2(c1);

        c1.start();
        c2.start();



    }
}
class Counter1 extends Thread{
    

    public void run(){

        for(int i=1; i<=100;i++){
            System.out.println("Numarator "+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
class Counter2 extends Thread{
    Thread t;
    Counter2(Thread t){this.t=t;}

    public void run(){
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(int i=101;i<=200;i++){
            System.out.println("Numarator "+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
