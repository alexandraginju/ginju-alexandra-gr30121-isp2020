package ginju.alexandra.lab2.ex1;
import java.util.Scanner;

public class Problema1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();

        if (x > y)
            System.out.println("x=" + x);
        else System.out.println("y=" + y);
    }
}
