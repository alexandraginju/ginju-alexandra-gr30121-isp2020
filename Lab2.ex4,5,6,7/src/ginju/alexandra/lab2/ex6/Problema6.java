package ginju.alexandra.lab2.ex6;
import java.util.Scanner;
//metoda recursiva
public class Problema6 {
    static int factorial( int n ) {
        if (n != 0)
            return n * factorial(n-1);
        else
            return 1;
    }

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int N=in.nextInt();
        int number = N, result;
        result = factorial(number);
        System.out.println(number + " factorial = " + result);
    }
}