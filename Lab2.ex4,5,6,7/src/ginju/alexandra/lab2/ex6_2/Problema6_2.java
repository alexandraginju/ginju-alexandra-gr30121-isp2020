package ginju.alexandra.lab2.ex6_2;
import java.util.Scanner;
//metoda nerecursiva

public class Problema6_2 {
    static int factorial1(int n){

        int p=1;
        while(n!=0) {
            p=p*n;
            n=n-1;

        }
        return p;
    }
    public static void main(String[] args){
        Scanner in=new Scanner(System.in);
        int N=in.nextInt();
        int number=N, result;
        result=factorial1(number);
        System.out.println(number+" factoriall="+result);
    }
}
