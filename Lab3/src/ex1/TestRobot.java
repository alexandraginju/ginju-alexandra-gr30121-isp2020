package ex1;

class Robot {
    int x;//position

    Robot() {
        x = 1;
    }

    void change(int k) {
        if (k >= 1)
            x = x + k;
    }

    public String toString() {
        return "pozitia="+x;
    }
}
public class TestRobot {
    public static void main(String[] args) {
        Robot r = new Robot();
        r.change(3);
        System.out.println(r);

    }
}


