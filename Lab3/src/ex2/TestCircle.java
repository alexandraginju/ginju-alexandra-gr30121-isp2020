package ex2;
import java.lang.Math.*;

public class TestCircle {
    public static void main(String[] args){
        Circle c1=new Circle();
        c1.getRadius();
        c1.getArea();
        Circle c2=new Circle("blue");
        //c2.getRadius();
        c2.getArea();


    }

}
class Circle{
    private double radius=1.0;
    private String color="red";
    Circle(){
        System.out.println("culoarea este:"+color);
    }
    Circle(String color){
        System.out.println("Culoare este:"+color);
    }
    void getRadius(){
        radius=2.0;
    }
    void getArea(){
        System.out.println("Aria="+Math.PI*radius*radius);
    }

}
