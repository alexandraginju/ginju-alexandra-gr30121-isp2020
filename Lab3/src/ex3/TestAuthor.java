package ex3;

public class TestAuthor {
    public static void main(String[] args){
        Author a1=new Author("My Name","myname@somewhere.com",'m');
        System.out.println(a1);
        a1.setEmail("abc@dot.com");
        System.out.println("name is: " + a1.getName());
        System.out.println("eamil is: " + a1.getEmail());
        System.out.println("gender is: " + a1.getGender());

    }

}
class Author{
    private String name;
    private String email;
    private char gender;
    public Author(String name, String email, char gender){
        this.name=name;
        this.email=email;
        this.gender=gender;
    }
    public String getName(){
        return name;
    }
    public String getEmail(){
        return email;
    }
    public void setEmail(String newEmail){
        this.email=newEmail;
    }
    public char getGender(){
        return gender;
    }
    public String toString(){
        return this.name+"("+this.gender+") at "+this.email;
    }
}
