package ex4;
import java.lang.Math;
public class TestMyPoint {
    public static void main(String[] args){
        MyPoint p1=new MyPoint(0,3);
        MyPoint p2=new MyPoint(0,4);
        System.out.println(p1);
        System.out.println(p1.distance(5,6));
        System.out.println(p1.distance(p2));
    }
}
class MyPoint{
    private int x;
    private int y;
     MyPoint(){
        x=0;
        y=0;
    }
     MyPoint(int x, int y){
        this.x=x;
        this.y=y;
    }
     int getX(){
        return x;
    }
     void setX(int newx){
        this.x=newx;
    }
     int getY(){
        return y;
    }
     void setY(int newy){
        this.y=newy;
    }
    void setXY(int x,int y){
        this.x=x;
        this.y=y;
    }
     public String toString(){
        return "("+this.x+", "+this.y+")";
    }
    double distance(int x, int y){
        int n=this.x-x;
        int m=this.y-y;
        return Math.sqrt(n*n + m*m);
    }
    double distance(MyPoint another){
        int n=this.x-x;
        int m=this.y-y;
        return Math.sqrt(n*n + m*m);
    }


}
