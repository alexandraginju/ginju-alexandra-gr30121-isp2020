package ex5;

public class Flower {
    int petal;
    static int count;
   public Flower()
    {
        count++;
    }

    Flower(int p){
       count++;
        petal=p;
        System.out.println("New flower has been created!");
    }
    public void counter()
    {
        System.out.println(count);
    }
    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        Flower f3 = new Flower();
        f2.counter();
        //f3.counter();
        //f1.counter();
    }
}
