package ex1;
import java.lang.Math.*;

public class Test {
    public static void main(String[] args){
        Circle c1=new Circle();
        Circle c2=new Circle(2.0);
        System.out.println(c1.getRadius());
        System.out.println(c1.getArea());
        System.out.println(c2.getRadius());
        System.out.println(c2.getArea());

    }
}
 class Circle{
    private double radius=1.0;
    private String color="red";
    Circle(){

    }
    Circle(double radius){
        this.radius=radius;

    }
    public double getRadius(){
        return radius;

    }
    public double getArea(){
        return Math.PI*radius*radius;
    }
 }
