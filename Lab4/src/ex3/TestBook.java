package ex3;
import ex2.Author;

public class TestBook {
    public static void main(String[] args){
        Author author1 = new Author("Abc", "abc@yahoo.ro", 'm');
        Book book1 = new Book("java", author1, 30.90, 100);  // Test Book's Constructor
        System.out.println(book1);
        System.out.println("numele cartii este: "+book1.getName());
        System.out.println("autorul cartii este: "+book1.getAuthor().getName());
        System.out.println("pretul este: "+book1.getPrice());
        book1.setPrice(20.5);
        System.out.println("pretul redus este: "+book1.getPrice());
        System.out.println("cantitatea este: "+book1.getQtyInStock());
        book1.setQtyInStock(60);
        System.out.println("cantitatea ramasa este: "+book1.getQtyInStock());


    }
}
 class Book{
    private String name;
    private Author author;
    private double price;
    private int qtyInStock=0;

    public Book(String name, Author author, double price){
        this.name=name;
        this.author=author;
        this.price=price;

    }
    public Book(String name, Author author, double price, int qtyInStock){
        this.name=name;
        this.author=author;
        this.price=price;
        this.qtyInStock=qtyInStock;

    }
    public String getName(){
        return name;
    }
    public Author getAuthor(){
        return author;
    }
    public double getPrice(){
        return price;
    }
    public void setPrice(double price){
        this.price=price;
    }
    public int getQtyInStock(){
        return qtyInStock;
    }
    public void setQtyInStock(int qtyInStock){
        this.qtyInStock=qtyInStock;
    }
    public String toString(){
        return "\""+name+"\" by "+author;
    }
 }

