package ex5;

public class TestCircle2 {
    public static void main(String[] args){
        Cylinder c=new Cylinder();
        System.out.println("raza="+c.getRadius()+" aria="+c.getArea()+" inaltimea="+c.getHeight());
        System.out.println("volumul cilindrului este="+c.getVolume());
        Cylinder c1 = new Cylinder(3.0);
        System.out.println("raza="+c1.getRadius()+" aria="+c1.getArea()+" inaltimea="+c1.getHeight());
        System.out.println("volumul cilindrului este="+c1.getVolume());
        Cylinder c2=new Cylinder(3.0,2.0);
        System.out.println("raza="+c2.getRadius()+" aria="+c2.getArea()+" inaltimea="+c2.getHeight());
        System.out.println("volumul cilindrului este="+c2.getVolume());

    }
}
class Circle{
    private double radius=1.0;
    private String color="red";
    public Circle(){
        radius=1.0;
        color="red";

    }
    public Circle(double radius){
        this.radius=radius;

    }
    public double getRadius(){
        return radius;

    }
    public double getArea(){
        return Math.PI*radius*radius;
    }
}
class Cylinder extends Circle{
    private double height=1.0;
    public Cylinder(){
        super();
        height=1.0;
    }
    public Cylinder(double radius){
        super(radius);

    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height=height;
    }
     public double getHeight(){
        return height;
     }
     public double getVolume(){
        return super.getArea()*height;
     }

}
