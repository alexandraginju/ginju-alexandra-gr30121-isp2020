package ex6;

public class TestShape {
    public static void main(String[] args){
        Circle c1=new Circle();
        System.out.println("aria cercului este: "+c1.getArea());
        System.out.println("perimetrul cercului este: "+c1.getPerimeter());
        System.out.println(c1);
        Circle c2=new Circle(2.0,"blue",false);
        System.out.println("aria cercului este: "+c2.getArea());
        System.out.println("perimetrul cercului este: "+c2.getPerimeter());
        System.out.println(c2);
        Rectangle r1=new Rectangle(2.0,3.0);
        System.out.println("aria dreptunghiului este: "+r1.getArea());
        System.out.println("perimetrul dreptunghiului este: "+r1.getPerimeter());
        System.out.println(r1);
        Rectangle r2=new Rectangle(4.0,1.0, "yellow", true);
        System.out.println("aria dreptunghiului este: "+r1.getArea());
        System.out.println("perimetrul dreptunghiului este: "+r1.getPerimeter());
        System.out.println(r1);
        Square s1=new Square(4.0,"grey",true);
        System.out.println(s1);

    }
}
class Shape{
    private String color="red";
    private boolean filled=true;

    public Shape(){
        color="green";
        filled=true;
    }
    public Shape(String color, boolean filled){
        this.color=color;
        this.filled=filled;
    }
    public String getColor(){
        return color;
    }
    public void setColor(String color){
        this.color=color;
    }
    public boolean isFilled(){
        return filled;
    }
    public void setFilled(boolean filled){
        this.filled=filled;
    }
    public String toString(){
        return "a shape with color of "+color+" and "+filled;
    }
}
class Circle extends Shape{
    private double radius;
    public Circle(){
        radius=1.0;
    }
    public Circle(double radius){
        this.radius=radius;
    }
    public Circle(double radius, String color, boolean filled){
        super(color,filled);
        this.radius=radius;
    }
    public double getRadius(){
        return radius;
    }
    public void setRadius(double radius){
        this.radius=radius;
    }
    public double getArea(){
        return Math.PI*radius*radius;
    }
    public double getPerimeter(){
        return 2*Math.PI*radius;
    }
    public String toString(){
        return "a circle with radius="+radius+", which is a subclas of "+super.toString();
    }
}
class Rectangle extends Shape{
    private double width;
    private double length;
    public Rectangle(){
        width=1.0;
        length=1.0;
    }
    public Rectangle(double width, double length){
        this.width=width;
        this.length=length;
    }
    public Rectangle(double width, double length, String color, boolean filled){
        super(color,filled);
        this.width=width;
        this.length=length;
    }
    public double getWidth(){
        return width;
    }
    public void setWidth(double width){
        this.width=width;
    }
    public double getLength(){
        return length;
    }
    public void setLength(double leghth){
        this.length=length;
    }
    public double getArea(){
        return width*length;
    }
    public double getPerimeter(){
        return 2*width+2*length;
    }
    public String toString(){
        return "a rectangle with width="+width+" and length="+length+", which is a subclass of "+super.toString();
    }
}
class Square extends Rectangle{
    public Square(){
        super();
    }
    public Square(double side){
        super(side,side);
    }
    public Square(double side, String color, boolean filled){
        super(side,side,color,filled);
    }
    public double getSide(){
       return super.getWidth();
    }
    public void setSide(double side){
        super.setWidth(side);
    }
    public void setWidth(double side) {
        super.setWidth(side);
    }
    public void setLength(double side){
        super.setLength(side);
    }
    public String toString(){
        return "A square with side="+getSide()+" which is a subclass of "+super.toString();
    }

}


