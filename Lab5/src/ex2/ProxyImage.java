package ex2;

public class ProxyImage implements Image {

    private RealImage realImage;
    private String fileName;
    private RotatedImage rotatedImage;
    private int a;

    public ProxyImage(String fileName, int a) {
        this.fileName = fileName;
        this.a = a;
    }

    @Override
    public void display() {
        if (a == 1) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        } else {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
            }

            rotatedImage.display();
        }
    }
}