package ex2;

public class RotatedImage implements Image {
    private String fileName;
    public RotatedImage(String fileName){
        this.fileName=fileName;
    }
    public void display(){

        System.out.println("Displaying "+ fileName);
    }

}
