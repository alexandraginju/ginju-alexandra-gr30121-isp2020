package ex3;

public class Controller {
    private TemperatureSensor tempSensor;
    private LightSensor lightsensor;
    public Controller(TemperatureSensor tempSensor, LightSensor lightSensor){
        this.tempSensor=tempSensor;
        this.lightsensor=lightSensor;
    }
    public void control(){
        int time = 10; //second
        System.out.print("Scanning");
        try {
            for (int i=0; i<time ; i++) {
                System.out.println("Temperature value: "+tempSensor.readValue());
                System.out.println("Light value: "+lightsensor.readValue());
                Thread.sleep(1000);
                System.out.print(".");
            }
        } catch (InterruptedException ie)
        {
            Thread.currentThread().interrupt();
        }

    }
}
