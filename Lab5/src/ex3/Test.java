package ex3;

public class Test {
    public static void main(String[] args) {
        TemperatureSensor temper = new TemperatureSensor();
        LightSensor light = new LightSensor();
        Controller c = new Controller(temper,light);
        c.control();
    }
}
