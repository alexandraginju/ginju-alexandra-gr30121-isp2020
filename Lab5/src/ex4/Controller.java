package ex4;

import ex3.LightSensor;
import ex3.TemperatureSensor;

public class Controller {
    private static Controller contr=null;
    private TemperatureSensor tempSensor;
    private LightSensor lightsensor;
    private Controller(TemperatureSensor tempSensor, LightSensor lightSensor){
        this.tempSensor=tempSensor;
        this.lightsensor=lightSensor;
    }
    public static Controller getInstance(TemperatureSensor tempSensor, LightSensor lightSensor)
    {
        if (contr == null)
            contr = new Controller(tempSensor, lightSensor);

        return contr;
    }
    public void control(){
        int time = 10; //second
        System.out.print("Scanning");
        try {
            for (int i=0; i<time ; i++) {
                System.out.println("Temperature value: "+tempSensor.readValue());
                System.out.println("Light value: "+lightsensor.readValue());
                Thread.sleep(1000);
                System.out.print(".");
            }
        } catch (InterruptedException ie)
        {
            Thread.currentThread().interrupt();
        }

    }

}
