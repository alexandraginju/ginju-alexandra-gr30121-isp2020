package ex4;
import ex3.LightSensor;
import ex3.TemperatureSensor;

public class Test {
    public static void main(String[] args) {
        TemperatureSensor temper = new TemperatureSensor();
        LightSensor light = new LightSensor();
        Controller c = Controller.getInstance(temper,light);
        c.control();
    }
}
