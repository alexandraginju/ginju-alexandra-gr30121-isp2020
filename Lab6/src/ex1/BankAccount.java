package ex1;
import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;
    BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }
    public void withdraw(double amount){
        System.out.println("withdraws: "+amount);
    }
    public void deposit(double amount){
        System.out.println("deposit: "+amount);
    }
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof BankAccount)) {
            return false;
        }
        BankAccount bak = (BankAccount) o;
        return Objects.equals(owner, bak.owner) && balance == bak.balance;
    }
    public int hashCode() {
        return Objects.hash(owner, balance);
    }


    public static void main(String[] args){
        BankAccount ba=new BankAccount("isk", 100);
        BankAccount ba1=new BankAccount("isk", 100);
        System.out.println(ba.equals(ba1));
    }
}
