package ex2;
import java.util.*;
import java.util.Comparator;

public class BankAccount {
    private String owner;
    private double balance;
    BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }
    public void withdraw(double amount){
        System.out.println("withdraws: "+amount);
    }
    public void deposit(double amount){
        System.out.println("deposit: "+amount);
    }
    public double getBalance(){
        return this.balance;
    }
    public String getOwner(){
        return owner;
    }
    public String toString(){
        return this.owner+" "+this.balance;
    }
}
class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

    public void addAccounts(String owner, double balance) {
        BankAccount bak = new BankAccount(owner, balance);
        accounts.add(bak);
    }

    public void printAccounts() {
        accounts.sort(Comparator.comparing(BankAccount::getBalance));
        //Collections.sort(accounts);
        for (int i = 0; i < accounts.size(); i++)
            System.out.println(accounts.get(i));
    }

    public void printAccounts(double minBalance, double maxBalance) {
        //accounts.sort(Comparator.comparing(BankAccount::getBalance));
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getBalance() >= minBalance && accounts.get(i).getBalance() < maxBalance)
                System.out.println(accounts.get(i));
        }
    }

    public void getAllAccounts() {
        accounts.sort(Comparator.comparing(BankAccount::getOwner));
        for (int i = 0; i < accounts.size(); i++)
            System.out.println(accounts.get(i));
    }

    public static Comparator<BankAccount> StuNameComparator = new Comparator<BankAccount>() {

        @Override
        public int compare(BankAccount b1, BankAccount b2) {
            String ownerName1 = b1.getOwner();
            String ownerName2 = b2.getOwner();
            return ownerName1.compareTo(ownerName2);
        }


    };
}

