package ex3;
//defapt exercitiul 4

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;


class Word{
    private String name;
    Word(String name){
        this.name=name;
    }

    public String getName(){
        return name;
    }
}
class Definition{
    private String description;
    private Word word;
    public Word getWord(){
        return word;
    }
    public String getDescription(){
        return description;
    }
    Definition(){
    }
    Definition(String description){
        this.description=description;
    }

}
class Dictionary{
    HashMap<Word,Definition> dictionary=new HashMap<Word,Definition>();
    public void addWord(Word w, Definition d){
        if(dictionary.containsKey(w))
            System.out.println("Modific cuvant existent!");
        else
            System.out.println("Adauga cuvant nou.");
        dictionary.put(w, d);
    }
    public Definition getDefinition(Word w){
        return  dictionary.computeIfPresent(w,(word, definition) -> definition);
    }
    public void getAllWords(){
        for(int i=0;i<=dictionary.size();i++)
            System.out.println(dictionary.get(i).getWord());

    }
    public void getAllDefinition(){
        for(int i=0;i<dictionary.size();i++)
            System.out.println(dictionary.get(i).getDescription());

    }

}
public class ConsoleMenu{
    public static void main(String[] args) throws IOException {
        Dictionary dict = new Dictionary();
        char raspuns;
        String word, def;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("c - Cauta cuvant");
            System.out.println("l - Listeaza dictionar");
            System.out.println("e - Iesi");

            word = fluxIn.readLine();
            def= fluxIn.readLine();
            raspuns = word.charAt(0);

            switch(raspuns) {
                case 'a': case 'A':
                    System.out.println("Introduceti cuvantul:");
                    word = fluxIn.readLine();
                    if( word.length()>1) {
                        System.out.println("Introduceti definitia:");
                        def = fluxIn.readLine();
                        dict.addWord(new Word(word), new Definition(def));
                    }
                    break;
                case 'c': case 'C':
                    System.out.println("Cuvant cautat:");
                    word = fluxIn.readLine();
                    if( word.length()>1) {
                        Word x = new Word(word);
                        def = dict.getDefinition(x).toString();
                        if (def == null)
                            System.out.println("Cuvantul nu a fost gasit!");
                        else
                            System.out.println("Definitie:"+def);
                    }
                    break;
                case 'l': case 'L':
                    System.out.println("Afiseaza:");
                    dict.getAllWords();
                    dict.getAllDefinition();
                    break;

            }
        } while(raspuns!='e' && raspuns!='E');
        System.out.println("Program terminat.");
    }
}
