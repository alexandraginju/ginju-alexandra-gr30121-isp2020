package ex2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountCharacter {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(
                new FileReader("data.txt"));

        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.print("Enter a character:");
        char c = (char) stdin.read();
        String s = new String();
        int counter=0;
        while ((s = in.readLine()) != null) {
            try {

                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == c) {
                        counter++;
                    }
                }

            } finally {
            }
        }
        in.close();
        System.out.println("Number of times "+"\""+c+"\""+" appears: "+counter);
    }
}


