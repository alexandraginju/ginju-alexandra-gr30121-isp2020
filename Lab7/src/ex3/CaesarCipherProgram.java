package ex3;
import java.io.*;
import java.util.*;
public class CaesarCipherProgram {
    public static void main(String args[]) throws IOException {

        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        String linie;
        char raspuns;
        Scanner sc = new Scanner(System.in);
        System.out.println(" Input the plaintext message : ");
        String plaintext = sc.nextLine();

        do {
            System.out.println("Alege");
            System.out.println("e- Encrypt");
            System.out.println("d- Decrypt");
            System.out.println("i - Iesire");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);


            switch (raspuns) {
                case 'e':
                    PrintWriter out1 = new PrintWriter(
                            new BufferedWriter(new FileWriter("encrypt.enc")));

                    System.out.println(" Enter the value by which each character in the plaintext message gets shifted : ");
                    int shift = sc.nextInt();
                    String ciphertext = "";
                    char alphabet;
                    for (int i = 0; i < plaintext.length(); i++) {
                        // Shift one character at a time
                        alphabet = plaintext.charAt(i);

                        // if alphabet lies between a and z
                        if (alphabet >= 'a' && alphabet <= 'z') {
                            // shift alphabet
                            alphabet = (char) (alphabet + shift);
                            // if shift alphabet greater than 'z'
                            if (alphabet > 'z') {
                                // reshift to starting position
                                alphabet = (char) (alphabet + 'a' - 'z' - 1);
                            }
                            ciphertext = ciphertext + alphabet;
                        }

                        // if alphabet lies between 'A'and 'Z'
                        else if (alphabet >= 'A' && alphabet <= 'Z') {
                            // shift alphabet
                            alphabet = (char) (alphabet + shift);

                            // if shift alphabet greater than 'Z'
                            if (alphabet > 'Z') {
                                //reshift to starting position
                                alphabet = (char) (alphabet + 'A' - 'Z' - 1);
                            }
                            ciphertext = ciphertext + alphabet;
                        } else {
                            ciphertext = ciphertext + alphabet;
                        }

                    }
                    out1.println(" encrypt message : " + ciphertext);
                    out1.close();
                    break;

                case 'd':
                    PrintWriter out2 = new PrintWriter(
                            new BufferedWriter(new FileWriter("decrypt.dec")));

                    System.out.println(" Enter the shift value : ");
                    int shift1 = sc.nextInt();
                    String decryptMessage = "";
                    for (int i = 0; i < plaintext.length(); i++) {
                        // Shift one character at a time
                        char alphabet1 = plaintext.charAt(i);
                        // if alphabet lies between a and z
                        if (alphabet1 >= 'a' && alphabet1 <= 'z') {
                            // shift alphabet
                            alphabet1 = (char) (alphabet1 - shift1);

                            // shift alphabet lesser than 'a'
                            if (alphabet1 < 'a') {
                                //reshift to starting position
                                alphabet1 = (char) (alphabet1 - 'a' + 'z' + 1);
                            }
                            decryptMessage = decryptMessage + alphabet1;
                        }
                        // if alphabet lies between A and Z
                        else if (alphabet1 >= 'A' && alphabet1 <= 'Z') {
                            // shift alphabet
                            alphabet1 = (char) (alphabet1 - shift1);

                            //shift alphabet lesser than 'A'
                            if (alphabet1 < 'A') {
                                // reshift to starting position
                                alphabet = (char) (alphabet1 - 'A' + 'Z' + 1);
                            }
                            decryptMessage = decryptMessage + alphabet1;
                        } else {
                            decryptMessage = decryptMessage + alphabet1;
                        }
                    }
                    out2.println(" decrypt message : " + decryptMessage);
                    out2.close();
                    break;
            }

        }while(raspuns!='i');
    }
}



