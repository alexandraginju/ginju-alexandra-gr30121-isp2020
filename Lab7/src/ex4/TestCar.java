package ex4;

import java.io.*;

public class TestCar {
    public static void main(String[] args) throws IOException {
        System.out.println("Car model: ");
        BufferedReader mod = new BufferedReader(
                new InputStreamReader(System.in));
        String m=mod.readLine();
        System.out.println("Car price: ");
        BufferedReader pr = new BufferedReader(
                new InputStreamReader(System.in));
        double p=Double.valueOf(pr.readLine()).doubleValue();

        Car c=new Car(m, p);
    
        //System.out.println(c);

        c.save("carX");
        try {
            Car c1 = Car.load("carX");
            System.out.println(c1);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}

class Car implements Serializable{
    private String model;
    private double price;
    Car(String model, double price){
        this.model=model;
        this.price=price;
    }
    void save(String fileName) {
        try {
            ObjectOutputStream o =
                    new ObjectOutputStream(
                            new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Care saved in folder");
        } catch (IOException e) {
            System.err.println("Car cannot be saved");
            e.printStackTrace();
        }

    }

    static Car load(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(fileName));
        Car c = (Car) in.readObject();
        return c;
    }

    public String toString(){
        return "Car: model "+model+" and price "+price;
    }
}
