package ex4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class TestAhomeAutomation {
}

class ControlUnit{
    private int TEMPERATURE;
    private FireSensor fireSensor;
    private TemperatureSensor temperatureSensor;
    AlarmUnit alarmUnit;
    HeatingUnit heatingUnit;
    CoolingUnit coolingUnit;
    GSMUnit gsmUnit;

    public void controlEvent(FireEvent fe, TemperatureEvent te) throws IOException {

        if(fireSensor.checkSmokeStatus(fe)==true){
            alarmUnit.startAlarm();
            gsmUnit.callOwner();
        }

        temperatureSensor.checkTemperature(te);

    }
    public void writeLogs(String log){

    }
}

class FireSensor{
    public boolean checkSmokeStatus(FireEvent fe){
        if(fe.isSmoke()){
            return true;
        }
        else return false;
    }
}

class TemperatureSensor{
    HeatingUnit hu;
    CoolingUnit cu;
    public int checkTemperature(TemperatureEvent te) throws IOException {
        if(te.getVlaue()<20 && te.getVlaue()!=20)
            hu.startHeating();
        else te.getVlaue();

        if(te.getVlaue()>20 && te.getVlaue()!=20)
            cu.startCooling();
            else te.getVlaue();
        return 0;
    }
}

class AlarmUnit {
    private boolean alarm;

    public void startAlarm() throws IOException {
        PrintWriter out1 = new PrintWriter(
                new BufferedWriter(new FileWriter("system_logs.txt")));
        out1.println("Alarma pornita");
        alarm=true;

    }
}

class HeatingUnit{
    TemperatureEvent te;
    public void startHeating() throws IOException {
        PrintWriter out1 = new PrintWriter(
                new BufferedWriter(new FileWriter("system_logs.txt")));
        out1.println("Porneste Incalzirea");
        te=new TemperatureEvent(20);
        te.getVlaue();

    }
}

class CoolingUnit{
    TemperatureEvent te;
    public void startCooling() throws IOException {
        PrintWriter out1 = new PrintWriter(
                new BufferedWriter(new FileWriter("system_logs.txt")));
        out1.println("Porneste racirea");
        te=new TemperatureEvent(20);
        te.getVlaue();

    }
}

class GSMUnit{
    public void callOwner() throws IOException {
        PrintWriter out1 = new PrintWriter(
                new BufferedWriter(new FileWriter("system_logs.txt")));
        out1.println("Call Owner");

    }
}

