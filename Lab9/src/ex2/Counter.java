package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {
    JTextField text;
    JButton button;
    int count=0;

    Counter(){

        setTitle("Test login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,200);
        setVisible(true);

    }
    public void init(){
        this.setLayout(null);

        text=new JTextField(count);
        text.setEditable(false);
        text.setBounds(50,50,80, 20);

        button=new JButton("Count");
        button.setBounds(40,10,80, 20);
        button.addActionListener(new TestButton());

        add(button); add(text);

    }

    class TestButton implements ActionListener{
        public void actionPerformed(ActionEvent e){
            count++;
            text.setText(count+"");
        }
    }
    public static void main(String[] args){
        new Counter();
    }
}

