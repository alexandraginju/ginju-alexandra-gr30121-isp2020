package ex3;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;


public class FileName extends JFrame
{

    JTextArea ta;
    JButton button;
    JTextField text;
    String f;

    FileName()
    {
        this.setLayout(null);


        text=new JTextField();
        text.setBounds(10, 50, 80,20);
        add(text);

        button=new JButton("Display");
        button.setBounds(10,100,100,20 );
        button.addActionListener(new TestButton());
        add(button);

        ta = new JTextArea();
        ta.setEditable(false);
        ta.setBounds(10,150,150,80);
        add(ta);

        setSize(300,300);
        setVisible(true);
    }

    class TestButton implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            try {
                String t = FileName.this.text.getText();
                BufferedReader bf =
                        new BufferedReader(
                                new FileReader(new File(t)));
                String l = "";
                l = bf.readLine();
                    while (l != null) {
                        ta.append(l + "\n");
                        l = bf.readLine();
                    }
            }catch(Exception ex){ta.append("No such file\n");}


        }
    }

    public static void main(String args[])
    {
        new FileName();
    }
}
