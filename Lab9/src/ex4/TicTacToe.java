package ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe extends JFrame {

    String jucatorCurent;
    JButton[][] placa;
    boolean castigator;

    public TicTacToe() {
        super();
        setLayout(new GridLayout(3,3));
        setTitle("Tic Tac Toe");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        jucatorCurent="x";
        placa=new JButton[3][3];
        castigator=false;
        initializeBoard();

    }

    public void initializeBoard(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                JButton buton=new JButton();
                placa[i][j]=buton;
                buton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(castigator==false){
                            buton.setText(jucatorCurent);
                            winner();
                            togglePlayer();
                        }
                    }
                });
                add(buton);
            }
        }
    }
    public void togglePlayer(){
        if(jucatorCurent.equals("x"))
            jucatorCurent="0";
        else
            jucatorCurent="x";
    }
    public void winner(){
        if(placa[0][0].getText().equals(jucatorCurent)&& placa[1][0].getText().equals(jucatorCurent) && placa[2][0].getText().equals(jucatorCurent)) {
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[0][1].getText().equals(jucatorCurent)&& placa[1][1].getText().equals(jucatorCurent) && placa[2][1].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[0][2].getText().equals(jucatorCurent)&& placa[1][2].getText().equals(jucatorCurent) && placa[2][2].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[0][0].getText().equals(jucatorCurent)&& placa[0][1].getText().equals(jucatorCurent) && placa[0][2].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[1][0].getText().equals(jucatorCurent)&& placa[1][1].getText().equals(jucatorCurent) && placa[1][2].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[2][0].getText().equals(jucatorCurent)&& placa[2][1].getText().equals(jucatorCurent) && placa[2][2].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[0][0].getText().equals(jucatorCurent)&& placa[1][1].getText().equals(jucatorCurent) && placa[2][2].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }
        else if(placa[0][2].getText().equals(jucatorCurent)&& placa[1][1].getText().equals(jucatorCurent) && placa[2][0].getText().equals(jucatorCurent)){
            JOptionPane.showMessageDialog(null, "Jucatorul " + jucatorCurent + " a castigat");
            castigator = true;
        }

    }

    public static void main(String[] args){
        TicTacToe main=new TicTacToe();
        main.setSize(new Dimension(400, 300));
        main.setVisible(true);
    }

}
