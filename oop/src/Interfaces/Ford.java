package Interfaces;

public class Ford implements Vehicle {
    public void start(){
        System.out.println("Starting");
    }
    public void stop(){
        System.out.println("Stopping");
    }
    public void cruise(){
        System.out.println("Cruising");
    }

    public static void main(String[] args) {
        Ford f150=new Ford();
        f150.start();
        f150.cruise();
        f150.stop();

        //Vehicle ve=new Vehicle();
    }
}
