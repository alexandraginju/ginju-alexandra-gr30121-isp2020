package Interfaces;

public interface Vehicle {
    public void start();
    public void stop();
    public void cruise();
}
