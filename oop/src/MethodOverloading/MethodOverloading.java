package MethodOverloading;

public class MethodOverloading {
    public void method(){
        System.out.println("No argumnt");
    }
    public void method(int i){
        System.out.println("intiger arhument : "+i);
    }
    public void method(int i, int j){
        System.out.println("2 int arg");
    }
    public static void main(String[] args){
        MethodOverloading methodOverloading=new MethodOverloading();
        methodOverloading.method();
        methodOverloading.method(10);
    }

}
