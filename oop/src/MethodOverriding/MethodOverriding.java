package MethodOverriding;

public class MethodOverriding {
    public static void main(String[] args) {
        Parent parent=new Parent();
        parent.m1();

        Child child=new Child();
        child.m1(); //child method

        Parent parent1=new Child();
        parent1.m1(); //child method
    }
}
