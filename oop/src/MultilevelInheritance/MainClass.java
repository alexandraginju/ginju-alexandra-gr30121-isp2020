package MultilevelInheritance;

public class MainClass {
    public static void main(String[] args) {
        A a= new A();
        a.methodA();
        //a.methodB();/C

        B b=new B();
        b.methodB();
        b.methodA();
        //b.methodC();

        C c=new C();
        c.methodC();
        c.methodA();
        c.methodB();
    }
}
