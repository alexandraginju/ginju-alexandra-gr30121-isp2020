package emailapp;

import java.util.Scanner;

public class Email {
    private String firstName;
    private String lastName;
    private String password;
    private String departament;
    private String email;
    private int mailboxCapacity=500;
    private int defaultPasswordLength;
    private String alternateEmail;
    private String companySuffix="aeycompany.com";

    //Constructor to receive the first and last name
    public Email(String firstName, String lastName){
        this.firstName=firstName;
        this.lastName=lastName;
        System.out.println("EMAIL CREATED: "+this.firstName+" "+this.lastName);

        //Call a method asking for the department - return the department
        this.departament=setDepartment();
        //System.out.println("Department: "+this.departament);

        //Call a method that returns a random pass
        this.password=randomPassword(8);
        System.out.println("Yoyr password is : "+this.password);

        //Combine elemenr to generate email
        email=firstName.toLowerCase()+"."+lastName.toLowerCase()+"@"+departament+"."+companySuffix;
        //System.out.println("Your email is: "+email);
    }

    //Ask for departament
    private String setDepartment(){
        System.out.println("DEPARTMENT CODE\n1 for Sales\n2 for Development\n3 for Accounting\n0 for none\nEnter department code");
        Scanner in=new Scanner(System.in);
        int depChoice=in.nextInt();
        if(depChoice==1) {
            return "sales";
        }
        else if(depChoice==2) {
            return "dev";
        }
        else if(depChoice==3){
            return "acct";
        }
        else {
            return "";
        }
    }

    //Generate random pass
    private String randomPassword(int length){
        String passwordSet="ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890!@#$%";
        char[] password=new char[length];
        for(int i=0; i<length;i++){
          int rand=  (int)(Math.random()*passwordSet.length());
          password[i]=passwordSet.charAt(rand);
        }
        return new String(password);
    }

    //Set the mailbox capacity
    public void setMailboxCapacity(int capacity){
        this.mailboxCapacity=capacity;
    }

    //Set the alternate email
    public void setAlternateEmail(String altEmail){
        this.alternateEmail=altEmail;
    }

    //Change the pass
    public void changePassword(String password){
        this.password=password;
    }

    public int getMailboxCapacity() {
        return mailboxCapacity;
    }

    public String getAlternateEmail() {
        return alternateEmail;
    }

    public String getPassword() {
        return password;
    }
    public String showInfo(){
        return "DISPLAY NAME: "+ firstName+" "+lastName +
                "COMPANY EMAIL: " +email+
                "MAILBOX CAPACITY: "+mailboxCapacity+ "mb";
    }
}
